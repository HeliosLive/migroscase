export class ProductModal {
    productId:number;
    categoryId:number;
    productName:string;
    imageUrl:string;
    quantityPerUnit:string;
    oldUnitPrice:number;
    unitPrice:number;
    unitInStock:number;
    howManyAdded:number;
    }