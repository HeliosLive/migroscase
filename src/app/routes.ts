import { Routes } from "@angular/router";
import { HomeComponent } from "../app/home/home.component";
import { ProductComponent } from "../app/product/product.component";
import { ShoppingListComponent } from "./shopping-list/shopping-list.component"; 

export const appRoutes: Routes = [
  { path: "home", component: HomeComponent },
  { path: "products", component: ProductComponent },
  { path: "shopping", component: ShoppingListComponent },
  { path: "**", redirectTo: "home", pathMatch: "full" }
];
