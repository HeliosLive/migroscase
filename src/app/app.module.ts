import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NgModuleFactoryLoader, SystemJsNgModuleLoader} from '@angular/core';
import { RouterModule, Router, PreloadAllModules } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms"; 
import { HashLocationStrategy, LocationStrategy } from '@angular/common'; 
import { StoreModule } from '@ngrx/store';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ProductComponent } from './product/product.component';
import { ShoppingListComponent } from './shopping-list/shopping-list.component';
 
import { appRoutes } from "./routes";
import { HeaderComponent } from './home/header/header.component';
import { FooterComponent } from './home/footer/footer.component';
import { shoppingListReducer } from './shopping-list/store/shopping-list.reducers';
import { AlertifyService } from './services/alertify/alertify.service';


@NgModule({
   declarations: [
      AppComponent,
      HomeComponent,
      ProductComponent,
      ShoppingListComponent,
      HeaderComponent,
      FooterComponent
   ],
   imports: [
      BrowserModule,      
      FormsModule,
      ReactiveFormsModule,
      RouterModule.forRoot(appRoutes, {preloadingStrategy: PreloadAllModules}),
      StoreModule.forRoot({shoppingList: shoppingListReducer})
   ],
   providers: [
      AlertifyService,
      {provide: LocationStrategy, useClass: HashLocationStrategy},
      { provide: NgModuleFactoryLoader, useClass: SystemJsNgModuleLoader }
   ],   //F5 te hata yememesi için LocationStrategy
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
