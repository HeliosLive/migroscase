import { Action } from '@ngrx/store';

import { ProductModal } from '../../../models/productModal';

export const PRICES_INGREDIENT = 'PRICES_INGREDIENT';
export const ADD_INGREDIENT = 'ADD_INGREDIENT';
export const ADD_INGREDIENTS = 'ADD_INGREDIENTS';
export const UPDATE_INGREDIENT = 'UPDATE_INGREDIENT';
export const DELETE_INGREDIENT = 'DELETE_INGREDIENT';

export class PricesIngredient implements Action {
  readonly type = PRICES_INGREDIENT;

  constructor() {}
}

export class AddIngredient implements Action {
    readonly type = ADD_INGREDIENT;
  
    constructor(public payload: ProductModal) {}
  }

export class AddIngredients implements Action {
  readonly type = ADD_INGREDIENTS;

  constructor(public payload: ProductModal[]) {}
}

export class UpdateIngredient implements Action {
  readonly type = UPDATE_INGREDIENT;

  constructor(public payload: {index: number, ingredient: ProductModal}) {}
}

export class DeleteIngredient implements Action {
  readonly type = DELETE_INGREDIENT;

  constructor(public payload: number) {}
}

export type ShoppingListActions =
PricesIngredient |
  AddIngredient |
  AddIngredients |
  UpdateIngredient |
  DeleteIngredient;
