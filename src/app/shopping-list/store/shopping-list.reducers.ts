import * as ShoppingListActions from './shopping-list.actions';

import { ProductModal } from '../../../models/productModal';
import { CartItem } from '../../../models/cartItem'; 

const initialState = {
  ingredients: [
  ]
};


export function shoppingListReducer(state = initialState, action: ShoppingListActions.ShoppingListActions) {
  switch (action.type) {
    case ShoppingListActions.ADD_INGREDIENT:
    const _Ingredients = [...state.ingredients];
    debugger;
    var exist :number=-1;
      for (let i = 0; i < _Ingredients.length; i++) {
        const element = _Ingredients[i];
        if(element.productId == action.payload.productId)
        {
          exist = i;
          break;
        }
      } 
    if(exist >= 0){
      _Ingredients[exist].howManyAdded += 1;
      return {
        ...state,
        ingredients: [..._Ingredients]
      };
    }
    else{
      return {
        ...state,
        ingredients: [...state.ingredients, action.payload]
      };
    } 
    case ShoppingListActions.ADD_INGREDIENTS:
      return {
        ...state,
        ingredients: [...state.ingredients, ...action.payload]
      };
    case ShoppingListActions.UPDATE_INGREDIENT:
      const ingredient = state.ingredients[action.payload.index];
      const updatedIngredient = {
        ...ingredient,
        ...action.payload.ingredient
      };
      const ingredients = [...state.ingredients];
      ingredients[action.payload.index] = updatedIngredient;
      return {
        ...state,
        ingredients: ingredients
      };
    case ShoppingListActions.DELETE_INGREDIENT:
      const oldIngredients = [...state.ingredients];
      oldIngredients.splice(action.payload, 1);
      return {
        ...state,
        ingredients: oldIngredients
      };
    default:
      return state;
  } 
}
 