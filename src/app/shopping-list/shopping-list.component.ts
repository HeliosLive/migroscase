import { Component, OnInit ,OnChanges} from '@angular/core';
import { CartItem } from 'src/models/cartItem';
import { ShoppingcartService } from '../services/shopping-cart/shoppingcart.service';
import * as ShoppingListActions from '../shopping-list/store/shopping-list.actions';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store'; 
import { Observable } from 'rxjs';
import { ProductModal } from 'src/models/productModal'; 
import { AlertifyService } from '../services/alertify/alertify.service';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.scss']
})
export class ShoppingListComponent implements OnInit {

  constructor(
    private cartService: ShoppingcartService,
    private alertifyService: AlertifyService,
    private router:Router,
    private store : Store<{shoppingList:{ingredients:ProductModal[]}}>
    ) { }
 
  totalPrice : number = 0;
  PriceElement : any ;
  // cartItems: CartItem[] = [];
  shoppingListState: Observable<{ingredients:ProductModal[]}>;
   
  ngDoCheck() {     
    this.priceCalculate();
  }
 
  ngOnInit() {
    // this.cartItems = this.cartService.list();   Eski servis yapısı 
    this.shoppingListState = this.store.select('shoppingList');   // ngRx yapısı !!
    this.shoppingListState.forEach(element => { 
      this.PriceElement = element.ingredients;
    }); 
    this.priceCalculate();
  }
 
  increaseFromCart(product:ProductModal,index:number){ 
      this.alertifyService.success(product.productName + " adedi 1 arttırıldı !");
      product.howManyAdded += +1;
      this.store.dispatch(new ShoppingListActions.UpdateIngredient({index:index,ingredient:product}));  // yeni ngRx yapısı !!
  }
  decreaseFromCart(product:ProductModal,index:number){
    if(product.howManyAdded > 1){
      this.alertifyService.warning(product.productName + " adedi 1 azaltıldı !");
      product.howManyAdded += -1;
      this.store.dispatch(new ShoppingListActions.UpdateIngredient({index:index,ingredient:product}));  // yeni ngRx yapısı !!
    }
    else{
      this.alertifyService.error(product.productName + " Sepetten Çıkartılmıştır !");
      this.deleteItem(index);
    }
  } 

  priceCalculate(){
    this.totalPrice=0;
    this.PriceElement.forEach(element => { 
      this.totalPrice += element.howManyAdded * element.unitPrice ; 
    });
  }

  deleteItem(index : number,product?:ProductModal){
    this.store.dispatch(new ShoppingListActions.DeleteIngredient(index));
    this.alertifyService.error(product.productName + " Sepetten Çıkartılmıştır !");
  }
  goProducts(){     
    this.router.navigate(["/products"]);  
   }

}
