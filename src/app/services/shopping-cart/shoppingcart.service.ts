import { Injectable } from '@angular/core';
import { ProductModal } from 'src/models/productModal';
import { CartItem } from 'src/models/cartItem';


export const CART_ITEM_LIST : CartItem[]=[]; 

@Injectable({
  providedIn: 'root'
})
export class ShoppingcartService {

constructor() { }

addToCart(product: ProductModal): void {
  var addedItem = CART_ITEM_LIST.find(t => t.product.productId == product.productId)
  if (addedItem) {
    addedItem.quantity += 1; 
  }
  else {
    let cartItem = new CartItem();
    cartItem.product = product;
    cartItem.quantity = 1;
    CART_ITEM_LIST.push(cartItem);
    console.log('CART_ITEM_LIST : ', CART_ITEM_LIST);
  }
}

list(): CartItem[] { 
  console.log(CART_ITEM_LIST);
  return CART_ITEM_LIST;
}
  
clear() {
  CART_ITEM_LIST.splice(0, CART_ITEM_LIST.length);
}

removeFromCart(product: ProductModal) {
  var addedItem = CART_ITEM_LIST.find(t => t.product.productId == product.productId)
  var indexNo = CART_ITEM_LIST.indexOf(addedItem);
  if (indexNo != -1) {
    CART_ITEM_LIST.splice(indexNo, 1);
  }

}

decreaseFromCart(product: ProductModal) {
  var Item = CART_ITEM_LIST.find(t => t.product.productId == product.productId)
  if (Item.quantity > 1) {
    Item.quantity -= 1;
  }
  else {
    alert("This Product Completely Removed From Cart");
    this.removeFromCart(product);
  }
}


}
