import { Component } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router'
import { CartItem } from 'src/models/cartItem';
import { ShoppingcartService } from './services/shopping-cart/shoppingcart.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'migros-case-spa';
  
  constructor(private cartService: ShoppingcartService,private router:Router) { }

  cartItems: CartItem[] = [];
  
  // ngOnInit() {
  //   this.cartItems = this.cartService.list();
  // }
  
  // goHome(){    
  //   this.router.navigate(['/home'])  
  //   // location.reload();
  // }
  // goProducts(){      
  //   this.router.navigate(['/products'])   
  // }
  // goShoppingCart(){   
  //   this.router.navigate(['/shopping'])   
  // }
}
