import { Component, OnInit } from '@angular/core';
import { ProductModal } from 'src/models/productModal';
import { ShoppingcartService } from '../services/shopping-cart/shoppingcart.service';
import { Store } from '@ngrx/store';
import * as ShoppingListActions from '../shopping-list/store/shopping-list.actions';
import { AlertifyService } from '../services/alertify/alertify.service';

declare var require: any;
var MockProduct = require('./../../Product.json'); 

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  constructor(private cartService: ShoppingcartService,
    private alertifyService: AlertifyService,
    private store : Store<{shoppingList:{ingredients:ProductModal[]}}>) { }
  
  product:ProductModal;
  products:ProductModal[] = [];

  ngOnInit() {
    this.loadScripts();
   this.getProducts();  //mock data çekiliyor..
  }

  addToCart(product : ProductModal){    
    product.howManyAdded = (product.howManyAdded != undefined) ? product.howManyAdded  : 1;  
    this.store.dispatch(new ShoppingListActions.AddIngredient(product));  // yeni ngRx yapısı !!
    this.alertifyService.success(product.productName + " sepete eklenmiştir !");
    // this.cartService.addToCart(product); // eski service yapısı
  }
  // mock data olarak oluşturulan ürünler listelenmesi için json dosyasından çekilir..
  // mock json olmasaydı ngRx ile de yapılabilirdi.
  getProducts(){  
    MockProduct.products.data.forEach(element => {
      this.product = new ProductModal();
      this.product.productId = element.productId;
      this.product.productName = element.productName;
      this.product.categoryId = element.categoryId;
      this.product.imageUrl = element.imageUrl;
      this.product.quantityPerUnit = element.quantityPerUnit;
      this.product.unitInStock = element.unitInStock;
      this.product.unitPrice = element.unitPrice;
      this.product.oldUnitPrice = element.oldUnitPrice;
      this.products.push(this.product);
    }); 

} 
    loadScripts() {
      const dynamicScripts = [
      './assets/vendor/owl.carousel/owl.carousel.min.js',
      './assets/vendor/owl.carousel2.thumbs/owl.carousel2.thumbs.js',
      './assets/js/front.js',
      './assets/js/flyto.js'
      ];
      for (let i = 0; i < dynamicScripts.length; i++) {
        const node = document.createElement('script');
        node.src = dynamicScripts[i];
        node.type = 'text/javascript';
        node.async = false;
        node.charset = 'utf-8';
        document.getElementsByTagName('head')[0].appendChild(node);
      }
    }  

}
