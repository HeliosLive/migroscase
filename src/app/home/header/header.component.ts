import { Component, OnInit } from "@angular/core";
import { CartItem } from "src/models/cartItem";
import { ShoppingcartService } from "src/app/services/shopping-cart/shoppingcart.service";
import { Router } from "@angular/router";
import { Observable } from "rxjs";
import { ProductModal } from "src/models/productModal";
import { Store } from "@ngrx/store";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"]
})
export class HeaderComponent implements OnInit {
  constructor(
    private cartService: ShoppingcartService,
    private router: Router,
    private store: Store<{ shoppingList: { ingredients: ProductModal[] } }>
  ) {}

  // cartItems: CartItem[] = [];
  shoppingListState: Observable<{ ingredients: ProductModal[] }>;

  ngOnInit() {
    this.shoppingListState = this.store.select("shoppingList"); // ngRx yapısı !!
    // this.cartItems = this.cartService.list();  // eski servis yapısı
  }

  goHome() {
    this.router.navigate(["/home"]); 
  }
  goProducts() {
    this.router.navigate(["/products"]);
  }
  goShoppingCart() {
    this.router.navigate(["/shopping"]);
  }
}
