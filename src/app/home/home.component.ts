import { Component, OnInit } from '@angular/core';
import { ProductModal } from 'src/models/productModal';
import { Router } from '@angular/router';

declare var require: any;
var MockProduct = require('./../../Product.json'); 

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private router:Router) { }
  
  product:ProductModal;
  products:ProductModal[] = [];

  ngOnInit() {
    this.getProducts();
    this.loadScripts();
   }
 
   goProducts(){     
    this.router.navigateByUrl("products");
   }
   // mock data olarak oluşturulan ürünler listelenmesi için json dosyasından çekilir..
   getProducts(){   
     MockProduct.products.data.forEach(element => {
       this.product = new ProductModal();
       this.product.productId = element.productId;
       this.product.productName = element.productName;
       this.product.categoryId = element.categoryId;
       this.product.imageUrl = element.imageUrl;
       this.product.quantityPerUnit = element.quantityPerUnit;
       this.product.unitInStock = element.unitInStock;
       this.product.unitPrice = element.unitPrice;
       this.products.push(this.product); 
     }); 
 
 } 

    loadScripts() {
      const dynamicScripts = [
      './assets/vendor/owl.carousel/owl.carousel.min.js',
      './assets/vendor/owl.carousel2.thumbs/owl.carousel2.thumbs.js',
      './assets/js/front.js',
      './assets/js/flyto.js'
      ];
      for (let i = 0; i < dynamicScripts.length; i++) {
        const node = document.createElement('script');
        node.src = dynamicScripts[i];
        node.type = 'text/javascript';
        node.async = false;
        node.charset = 'utf-8';
        document.getElementsByTagName('head')[0].appendChild(node);
      }
    }
}

 